# README #

HSP用メルセンヌツイスタモジュール

モジュールファイルをincludeするだけで使えます

### 関数の説明 ###



```
#!text

init_genrand　命令

乱数ジェネレータを初期化する。
int型の値を指定することで初期SEEDを設定することができる。

引数
int 初期SEED

返り値
なし

genrand_int32 関数
32bit　int型の整数を乱数値として取得する

引値
なし

返り値
乱数値

```



### 例 ###
```
#!HSP

//引数は現在時刻
init_genrand ( gettime(7) + 1000*( gettime(5) + 60*( gettime(4) + 24*gettime(3) ) ) )

//乱数ジェネレータで取得した値を描画する
mes "乱数値:" + genrand_int32() + "\n" 


```